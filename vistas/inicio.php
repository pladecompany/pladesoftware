<div id="Inicio" class="hero-slider mt-10vh vh90">
	<div class="slider-item slide1 relative bg-animated" style="">
		 <div class="text-hero w-100 row flex item-center content text-center">

		 <div class="col-lg-6">
			 <h1 class="text-white text-responsive" data-animation-in="slideInRight">Plade Software</h1>
				<p class="tag-text mb-4 ln" data-animation-in="slideInRight" data-duration-in="0.6">Solución a los procesos operativos diarios en todo negocio.  <span  class="bold" >  Sistema 100% Personalizable y Adaptable a tus Necesidades. </span> </p>
				<a href="#Contacto" class="btn btn-main btn-white" data-animation-in="slideInRight" data-duration-in="1.2">¡Solicitar demo!</a>
			 </div>
			 
			 <div class="col-lg-6 flex flex-center item-center">
					<img src="statics\images\slider-1.png" class="w-75" alt=""> 
			 </div>
		 </div>
	</div>

	<div class="slider-item vh90 relative bg-animated">
		<div class="text-hero w-100 row flex item-center content text-center">
			<div class="col-lg-6">
				<h1 class="text-white text-responsive" data-animation-in="slideInRight">¿Problemas a la hora del cierre?</h1>
				<p class="tag-text mb-4 ln " data-animation-in="slideInRight" data-duration-in="0.6">Imagina al final de cada día, facilitar el proceso de cierre, <b>siendo breve y preciso con Plade Software</b></p>
				<a href="https://api.whatsapp.com/send?phone=+584121548039&text=Quisiera%20informaci%C3%B3n%20sobre%20Plade%20Software" target="blank"  class="btn btn-main btn-white" data-animation-in="slideInRight" data-duration-in="1.2"><i class="fab fa-whatsapp"></i> Contactar</a>
			</div>

			<div class="col-lg-6 flex flex-center item-center">
				<img src="statics/images/slider-2.png" class="w-75" alt=""> 
			</div>
		</div>
	</div>
 
	<div class="slider-item vh90 relative bg-animated">
		<div class="text-hero w-100 row flex item-center content text-center">
			<div class="col-lg-6 ">
				<h1 class="text-white ln text-responsive" data-animation-in="slideInRight">Soluciona diferentes problemas administrativos hoy,  mantén el control de tus productos, ventas, compras, cuentas por cobrar </h1>
				<a href="#Contacto" class="btn btn-main btn-white mt-3" data-animation-in="slideInRight" data-duration-in="1.2">¡Solicitar demo!</a>
			</div>

			<div class="col-lg-6 flex flex-center item-center">
				<img src="statics\images\slider-3.png" class="w-75" alt=""> 
			</div>
		</div> 
	</div>
</div>

<?php include_once("empresas.php"); ?>
<?php include_once("modulos.php"); ?>


<div class="mb-5"  id="Plade_app"></div>

<section class="section pb-0 pt-0">
	<div class="row bg-azul-oscuro p-5 pb-0 flex flex-center item-end"> 
		<div class="text-center">
			<h3 class="text-white m-0 p-0">Integración al sistema de una app móvil y una página web</h3>
			<p class="text-white">Al tener <b>Plade Software</b> puedes solicitar el paquete extra con la integracion de una app móvil y/o una pagina web Personalizables</p>
			<a href="#Contacto" class="btn-header mt-3 mb-3">¡Quiero saber más!</a>

			<div class="w-100 mt-4">
				<img src="statics\images\banner-ps.png" class="img-banner" alt="">
			</div>
		</div>
	</div>
</section>

<!-- Impresoras fiscales -->


<section class="section pb-0 px-3">
	<div class="row">
		<div class="section-title col-12 text-center mb-4">
			<h3 class="mb-0" >Impresoras fiscales compatibles</h3>
			<!-- <p class="mb-0" >estamos autorizados por the factory HKA y POS VENEZUELa para sincronizar nuestro software con sus <b>maquinas fiscales.</b> </p> -->
		</div>
	</div>

	<div   class="main-carousel mb-5" data-flickity='{ "cellAlign": "left", "contain": true , "wrapAround": true , "freeScroll": true, "groupCells": true }'>
		<div class=" cell-2">
			<a href="#" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/impresoras/1.2.jpg" class="w-100 soft-border" alt="Image" >
			</a>
		</div>

		<div class=" cell-2">
			<a href="#" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/impresoras/1.3.jpg" class="w-100 soft-border" alt="Image" >
			</a>
		</div>

		<div class=" cell-2">
			<a href="#" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/impresoras/1.4.jpg" class="w-100 soft-border" alt="Image" >
			</a>
		</div>

		<div class=" cell-2">
			<a href="#" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/impresoras/1.5.jpg" class="w-100 soft-border" alt="Image" >
			</a>
		</div>

		<div class=" cell-2">
			<a href="#" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/impresoras/1.6.jpg" class="w-100 soft-border" alt="Image" >
			</a>
		</div>


	</div>
</section>

<!-- Compatibilidad -->

<section class="section pt-0 pb-0" id="Plade_app">
	<div class="row bg-azul-oscuro py-5 px-3 flex flex-center item-center">
		<h3 class="text-white m-0 pt-0 pb-0 pl-2 pr-2 w-100 text-center">estamos autorizados por the factory HKA y POS VENEZUELa <br> para sincronizar nuestro software con sus <b>maquinas fiscales.</b></h3>
	</div>
</section>

<!-- Funciones -->
<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6 order-2-md">
				<div class="contents">
					<div class="section-title">
						<h3 class="mb-0">Funciones pensadas para tu comodidad</h3>
					</div>

					<div class="text">
						<p>Plade Software cuenta con funciones específicas para diferentes áreas que te permitirán personalizar el sistema de acuerdo a tus requerimientos.</p>	
					</div>

					<div class="row">
						<div class="col-12 col-md-6 col-lg-6">
							<ul class="content-list pl-0" >
								<li class="flex item-center" >
									<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
									<p class="mb-0 ml-2" >Pantalla de pedido.</p>
								</li>

								<li class="flex item-center" >
									<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
									<p class="mb-0 ml-2" >Carnicerías y afines.</p>
								</li>

								<li class="flex item-center" >
									<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
									<p class="mb-0 ml-2" >Pantalla para presentar productos.</p>
								</li> 

								<li class="flex item-center" >
									<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
									<p class="mb-0 ml-2" >Sincronización con mercadolibre.</p>
								</li>
							</ul>
						</div>

						<div class="col-12 col-md-6 col-lg-6">
							<ul class="content-list pl-0">
								<li class="flex item-center" >
									<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
									<p class="mb-0 ml-2" >Restaurantes: Vista de mesas y carta.</p>
								</li>

								<li class="flex item-center" >
									<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
									<p class="mb-0 ml-2" >Paquete control en línea.</p>
								</li> 

								<li class="flex item-center" >
									<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
									<p class="mb-0 ml-2" >Integración del sistema al servidor web.</p>
								</li>
							</ul>
						</div>
					</div>

					<div class="text-center">
						<a href="#Contacto" class="btn btn-plade text-white mt-4">¡Quiero saber más!</a>
					</div>
				</div>
			</div>

		<div class=" order-1-md col-lg-6">
			<img loading="lazy" class="img-fluid" src="statics\images\funciones-ps.png" alt="service-image">
		</div>
	</div>
</section>

<!-- paquetes -->
<?php include_once("paquetes.php"); ?>

<div class="mb-5 pb-3" id="App" >
</div>

<!-- Plade app -->
<section id="App" class="section pt-0  pb-0" >
	<div class="">
		<div class="row flex bg-azul-oscuro item-center mt-4 pt-5 pb-5">
			<div class="col-lg-12 col-sm-12"></div>

			<div class="col-lg-6 flex flex-center">
				<img loading="lazy" class="w-75" src="statics/images/banner-app-2.jpg" alt="service-image">
			</div>

			<div class="col-lg-6 text-center">
				<h3 class="m-0 p-0 text-white" >Plade app vendedores</h3>

				<div class="text-center mt-3 pl-4 pr-4">
					<p class="text-white" >Gestión de ventas y cobranzas. Especial para negocios con equipos de venta que necesitan agilizar el montando de pedidos y facilitar la verificación en inventario, sincronizado con el sistema administrativo.</p>
				</div>

				<div class="flex flex-center flex-colum-xs">
					<div class="p-3 text-center ml-2 mr-2 flex-center-xs">
						<div class="bg-white circle-app" ><i style="font-size:30px" class="far fa-thumbs-up  text-color"></i></div>
						<p class="text-white bold mt-2 w-100 text-center" >Eficiente</p>
					</div>

					<div class="p-3 text-center ml-2 mr-2 flex-center-xs">
						<div class="bg-white  circle-app" ><i style="font-size:30px" class="far fa-thumbs-up  text-color"></i></div>
						<p class="text-white bold mt-2 w-100 text-center" >Rápida</p>			
					</div>

					<div class="p-3 text-center ml-2 mr-2 flex-center-xs">
						<div class="bg-white  circle-app" ><i style="font-size:30px" class="far fa-thumbs-up  text-color"></i></div>
						<p class="text-white bold mt-2 w-100 text-center" >Innovadora</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Testimonios -->
<?php include_once("testimonios.php"); ?>

<!-- Banner demo gratuito -->
<section class="section pt-0 pb-0" id="Plade_app">
	<div class="row bg-azul-oscuro pt-5 flex flex-center item-center">
		<h3 class="text-white m-0 pt-0 pb-0 pl-2 pr-2 w-100 text-center">¿AUN NO HAS OBTENIDO TU DEMO GRATUITO?</h3>
		<a href="#Contacto" class="btn-header mt-5 mb-5" >¡Obtener mi demo gratuito!</a>
	</div>
</section>


<!-- Contacto -->
<section id="Contacto" class="appoinment-section section p-5 p1_xs bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">

				<h3 class="mt-3" >Contáctanos</h3>

			</div>
		</div>
		
		<div class="row flex flex-center soft-border shadow">
			<div style="z-index: 9;" class="col-sm-12 col-lg-4 row  soft-border p-0 bg-azul-oscuro flex_md">					
				<div class="w-100 text-center mt-2 p-4 pb-0">
					 <h4 class="text-white" >Tambien nos puedes encontrar en </h4>
				</div>
				
				<a href="https://api.whatsapp.com/send?phone=+584121548039&text=Quisiera%20informaci%C3%B3n%20sobre%20Plade%20Software" target="blank" class=" text-center mt-2 w-100-lg w-100-xs ml-4 text-white">		 
					<i style="font-size:30px" class="fab fa-whatsapp pr-2"></i>
					<p class=" mb-0 text-white" >+58 412-0545738</p>
				</a>

				<a href="https://instagram.com/pladesoftware?igshid=cbl52pn3falz" target="blank" class="ml-4 w-100-xs w-100-lg mt-2 text-center text-white">
					<i style="font-size:30px" class="fab fa-instagram pr-2"></i>
					<p class=" mb-0 text-white" >@pladesoftware</p>
				</a>

				<a href="https://pladecompany.com/#hero-area" target="blank" class="w-100-xs w-100-lg mt-2 ml-4 text-center text-white">
					<i style="font-size:30px" class="fas fa-desktop pr-2"></i>
					<p class=" mb-0 text-white" >www.pladecompany.com</p>
				</a>
			</div>

			<div class=" col-sm-12 col-lg-8 bg-white soft-border">
				<div class="contact-area pl-0">
					<form name="contact_form" id="form_contacto" class="default-form contact-form php-email-form" action="controlador/contacto.php" method="post">
						<div class="row flex flex-center">
							<div class="col-sm-12 flex flex-center row item-center mb-3">
								<h4 class="clr-azul m-0 p-0  text-center mb-4 col-lg-12" >¿TIENES ALGUNA CONSULTA? </h4>
								<h5 class="text-black col-lg-12 text-center" >Ingresa los datos y serás contactado por uno de nuestros asesores </h5>
							</div>

							<div class="col-md-12 row flex flex-center">
								<div class="col-12 col-md-6 col-lg-6  mt-2 form-group">
									<p class="mb-0 ml-4 " >Nombre</p>
									<input class="form-control" type="text" name="nombre" placeholder="Ingrese su nombre" required="">
								</div>

								<div class="col-12 col-md-6  col-lg-6  mt-2 form-group">
									<p class="mb-0 ml-4 " >Apellido</p>
									<input class="form-control" type="text" name="apellido" placeholder="Ingrese su apellido" required="">
								</div>

								<div class="col-12 col-md-6  col-lg-6 mt-2 form-group">
									<p class="mb-0 ml-4 " >Correo</p>
									<input class="form-control" type="text" name="correo" data-rule="email" placeholder="Ingrese su correo" required="">
								</div>

								<div class="col-12 col-md-6  col-lg-6 mt-2 form-group">
									<p class="mb-0 ml-4 " >Whatsapp</p>
									<input class="form-control" type="text" name="whatsapp" placeholder="Ingrese su whatsapp" required="">
								</div>

								<div class="col-12 col-md-6  col-lg-6 mt-2">
									<p class="mb-0 ml-4" >Asunto</p>
									<select class="form-control " style="padding-left: 0px !important;" name="asunto">
										<option>Tengo dudas</option>
										<option>Quiero asesoria</option>
										<option>Quiero mi demo</option>
									</select>
								</div>

								<div style="align-items:flex-end;" class="col-12 col-md-6  col-lg-6  flex flex-center align-items">
									<div class=" mb-2 mt-4 text-center">
										<div class="loading" style="display: none;">Cargando</div>
										<div class="error-message" style="display: none;"></div>
										<div class="sent-message" style="display: none;">Tu mensaje ha sido enviado</div>

										<button class="btn-plade tag-inicio">Contactar <i class="fas fa-angle-double-right"></i> </button>
									</div>
								</div>
							</div>

							<div class="col-md-12 mt-5"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">

	$('#form_contacto').on('submit', function(evt){
		evt.preventDefault();
		const formData = new FormData(this);
		
		$('.loading').slideDown();

		$.ajax({
			url: 'controlador/contacto.php',
			data: formData,
			processData: false,
		})
		.done(function(res) {
			console.log(res);
			$('.loading').slideUp();
			$('#form_contacto button').attr('disabled', true);
			document.getElementById('form_contacto').reset();
		})
		.fail(function(res) {
			console.error(res);
		});
	});

</script>