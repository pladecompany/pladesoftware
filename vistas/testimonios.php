
<section class="section pt-3 pb-3 bg-gray mb-2 p1_xs">
	<div class="container">
		<div class="row">
			<div class="section-title col-12 text-center mb-4">
				<h3 class="mb-0 p-0" >Testimonios</h3>
			</div>
		</div>

		<div id="Inicio" style="height: min-content !important;" class="hero-slider pb-5">
			<div class="slider-item slide1 relative" style="height: min-content !important;">
				<div class=" w-100 row flex flex-center item-center">
					<div class="col-lg-12 text-center mt-4">
						<h2 class="" >Ferreteria reigo</h2>
					</div>

					<div class="col-lg-3 col-sm-12 col-xs-12 col-md-12 flex flex-end flex-center-md mt-3 mb-3">
						<img src="statics\images\logos\fondo.png" class="big-circle" alt="">
					</div>

					<div class="col-lg-6">
						<p>Recomendamos al 100% este software, permite ahorrar tiempo al momento de facturar y tiene muchas herramientas para llevar el control administrativo de nuestra empresa. En reigo trabajamos con Plade</p>
					</div>
				</div>
			</div>
				
			<div class="slider-item  relative " style="height: min-content !important;" >
				<div class=" w-100 row flex flex-center item-center ">
					<div class="col-lg-12 text-center mt-4">
						<h2 class="" >Bodegon punta brava</h2>
					</div>

					<div class="col-lg-3 col-sm-12 col-xs-12 col-md-12 flex flex-end flex-center-md  mt-3 mb-3">
						<img src="statics\images\logos\7.jpg" class="big-circle" alt="">
					</div>

					<div class="col-lg-6">
						 <p>Nuestra metodología de trabajo se tornaba compleja; debido a que el control de nuestro establecimiento es de forma manual. Actualmente, con la llegada de "Plade Software" (lo nuevo en tecnología) nuestro sistema de trabajo es de manera rápida y eficaz, a comodidad de nuestra clientela.</p>
					</div>
				</div>
			</div>
			
			<div class="slider-item  relative" style="height: min-content !important;" >
				<div class=" w-100 row flex flex-center item-center">
		
					<div class="col-lg-12  text-center mt-4">
						<h2 class="" >Cocofresh</h2>
					</div>

					<div class="col-lg-3 col-sm-12 col-xs-12 col-md-12 flex flex-end flex-center-md   mt-3 mb-3">
						<img src="statics\images\logos\Cocofresh.png" class="big-circle" alt="">
					</div>

					<div class="col-lg-6">
						 <p>Gracias a plade company puedo llevar el control total de mi negocio. Todos los pedidos son tomados desde Plade Software, le dije adiós a los papelitos </p>
					</div>
						
				</div>
			</div>				 
		</div>
	</div>
</section>