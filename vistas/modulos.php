<section id="Modulos" class="service-section bg-gray">
	<div class="container">
		<div class="section-title text-center">
			<div class="row">
				<div class="section-title col-12 text-center mb-2 mt-4">
					<h3 class="mb-0" >Módulos disponibles en Plade Software</h3>
				</div>
			</div>
			 
			<div class="row">
				<div class="col-lg-12">
					<div class="items-container">
						<div class="item relative">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Configuración del sistema</h6>
									</a>
									
									<div class=" badge-number ">
										<p class="h5 text-color" ><i style="font-size: 30px;" class="fas fa-cogs"></i></p>
									</div>
								</div>
							</div>
						</div>

						<div class="item relative">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Cuentas</h6>
									</a>

									<div class="badge-number">
										<p class="h5 text-color"><i style="font-size: 30px;" class="fas fa-id-card"></i></p>
									</div>
								</div>
							</div>
						</div>

						<div class="item relative">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Clientes</h6>
									</a>
									
									<div class=" badge-number">
										<p class="h5 text-color" ><i style="font-size: 30px;" class="fas fa-user-friends"></i></p>
									</div>
								</div>
							</div>
						</div>

						<div class="item relative">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Inventario</h6>
									</a>

									<div class=" badge-number ">
										<p class="h5 text-color" ><i style="font-size: 30px;" class="fas fa-boxes"></i></p>
									</div>
								</div>
							</div>
						</div>

						<div class="item relative">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center" >
									<a href="service.html">
										<h6>Facturación</h6>
									</a>

									<div class=" badge-number">
										<p class="h5 text-color" ><i style="font-size: 30px;" class="fas fa-file-invoice"></i></p>
									</div>
								</div>
							</div>
						</div>


						<div class="item">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Almacenes</h6>
									</a>

									<div class=" badge-number">
										<p class="h5 text-color" ><i style="font-size: 30px;" class="fas fa-box"></i></p>
									</div>
								</div>
							</div>
						</div>

						<div class="item relative">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Compras</h6>
									</a>
									<div class="badge-number ">
										<p class="h5 text-color" ><i style="font-size: 30px;" class="fas fa-shopping-cart"></i></p>
									</div>
								</div>
							</div>
						</div>

						<div class="item relative">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Gastos</h6>
									</a>
									<div class=" badge-number ">
										<p class="h5 text-color" ><i style="font-size: 30px;" class="fas fa-dollar-sign"></i></p>
									</div>
								</div>
							</div>
						</div>

						<div class="item">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Despacho</h6>
									</a>

									<div class=" badge-number ">
										<p class="h5 text-color" ><i style="font-size: 30px;" class="fas fa-truck-loading"></i></p>
									</div>
								</div>
							</div>
						</div>

						<div class="item">
							<div class="inner-box mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Pedidos</h6>
									</a>
									<div class=" badge-number ">
										<p class="h4 text-color" ><i style="font-size: 30px;" class="fas fa-american-sign-language-interpreting"></i></p>
									</div>
								</div>
							</div>
						</div>

						<div class="item">
							<div class="inner-box  item-center mt-5">
								<div class="image-content text-center flex flex-center">
									<a href="service.html">
										<h6>Cuentas por cobrar</h6>
									</a>

									<div class=" badge-number">
										<p  class=" text-color"><i style="font-size: 30px;" class="fas fa-comments-dollar"></i></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>