<section id="Paquetes" class="service-tab-section section pt-0">
	<div class="outer-box clearfix">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="tabs mb-5">
						<ul class="nav nav-tabs justify-content-center" id="aboutTab" role="tablist">
							<li class="nav-item" role="presentation">
								<a class="nav-link btn-plade-no-active active" id="dormitory-tab" data-toggle="tab" href="#dormitory" role="tab" aria-controls="dormitory" aria-selected="true">Mini básico</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link btn-plade-no-active"  id="orthopedic-tab" data-toggle="tab" href="#orthopedic" role="tab" aria-controls="orthopedic" aria-selected="false">Básico</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link btn-plade-no-active" id="sonogram-tab" data-toggle="tab" href="#sonogram" role="tab" aria-controls="sonogram" aria-selected="false">Intermedio</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link btn-plade-no-active" id="x-ray-tab" data-toggle="tab" href="#x-ray" role="tab" aria-controls="x-ray" aria-selected="false">Premiun</a>
							</li>
						 
						</ul>
					</div>
				 
					<div class="tab-content" id="aboutTab">
						<div class="service-box tab-pane fade show active" id="dormitory">
							<div class="row">

								<div class="col-lg-6">
									<img loading="lazy" class="img-fluid" src="statics\images\mini-basico-ps.jpg" alt="service-image">
								</div>

								<div class="col-lg-6">
									<div class="contents">
										<div class="section-title">
											<h3 class="mb-0" >Gestión esencial de ventas</h3>
										</div>
										<div class="text">
											<p>Para pequeños negocios.</p>
											<p>Todos los paquetes incluyen asesoría por parte de nuestro equipo</p>
												
										</div>

										<div class="row">

										<div class="col-12 col-md-6 col-lg-6">

											<ul class="content-list">
											<li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Módulo de clientes.</p>
											</li>
											<li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Módulo de configuración del sistema.</p>
											</li> <li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Módulo de facturación.</p>
											</li> <li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Módulo de inventario.</p>
											</li> 
											</ul>

										</div>

										<div class="col-12 col-md-6 col-lg-6">

												<ul class="content-list">
												<li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Reportes.</p>
												</li> <li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Módulo de bancos y cuentas.</p>
												</li> <li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Herramientas.</p>
												</li>
												</ul>

										</div>

										</div>

										<div class="text-center">

										<a href="#Contacto" class="btn btn-plade text-white mt-4">¡Quiero saber más!</a>

										</div>

									</div>

								</div>
							</div>
						</div>
						
						<div class="service-box tab-pane fade" id="orthopedic">
							<div class="row">
								<div class="col-lg-6">
									<img loading="lazy" class="img-fluid" src="statics\images\basico-ps.jpg" alt="service-image">
								</div>
								<div class="col-lg-6">
									<div class="contents">
										<div class="section-title">
											<h3 class="mb-0" >Gestión y control básico de ventas</h3>
										</div>
										<div class="text">
											<p>Para pequeños y medianos negocios.</p>
											<p>Todos los paquetes incluyen asesoría por parte de nuestro equipo</p>
											 
										</div>

										<div class="row">

										<div class="col-12 col-md-6 col-lg-6">

											<ul class="content-list">
											<li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Módulo de clientes.</p>
											</li>
											<li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Módulo de configuración del sistema.</p>
											</li> <li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Módulo de facturación.</p>
											</li> <li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Módulo de inventario.</p>
											</li> 
											<li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Reportes.</p>
												</li>
											</ul>

										</div>

										<div class="col-12 col-md-6 col-lg-6">

												<ul class="content-list">
											 
												 <li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Módulo de bancos y cuentas.</p>
												</li>
												 <li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Herramientes.</p>
												</li>
												<li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2"> Módulo de compras.</p>
												</li>
												<li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Módulo de cuentas por cobrar.</p>
												</li>
												<li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Módulo de almacén.</p>
												</li>
												</ul>

										</div>

										</div>

										<div class="text-center">

										<a href="#Contacto" class="btn btn-plade text-white mt-4">¡Quiero saber más!</a>

										</div>
									 
									</div>
								</div>
							</div>
						</div>
					 
						<div class="service-box tab-pane fade" id="sonogram">
							<div class="row">
								<div class="col-lg-6">
									<img loading="lazy" class="img-fluid" src="statics\images\intermedio-ps.jpg" alt="service-image">
								</div>
								<div class="col-lg-6">
									<div class="contents">
									<div class="section-title">
											<h3 class="mb-0" >Gestión y control amplio de empresa</h3>
										</div>
										<div class="text">
											<p>Para negocios medianos con mayores requerimientos.</p>
											<p>Todos los paquetes incluyen asesoría por parte de nuestro equipo</p>
											 
										</div>

										<div class="row">

										<div class="col-12 col-md-6 col-lg-6">

											<ul class="content-list">
											<li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >9 módulos del básico.</p>
											</li>
											<li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Multi Almacenes.</p>
											</li>
											</ul>

										</div>

										<div class="col-12 col-md-6  col-lg-6">

												<ul class="content-list">
												<li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Gastos.</p>
												</li> <li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Despacho.</p>
												</li> 
											 
												</ul>

										</div>

										</div>

										<div class="text-center mt-5">

										<a href="#Contacto" class="btn btn-plade text-white mt-4">¡Quiero saber más!</a>

										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="service-box tab-pane fade" id="x-ray">

						<div class="row">
								<div class="col-lg-6">
									<img loading="lazy" class="img-fluid" src="statics\images\integraciones-ps.jpg" alt="service-image">
								</div>
								<div class="col-lg-6">
									<div class="contents">
									<div class="section-title">
											<h3 class="mb-0" >Paquete completo</h3>
										</div>
										<div class="text">
											<p>Solución completa para la gestión, administración y ventas en línea por medio de tienda web y móvil sincronizados al sistema.</p>
											<p>Todos los paquetes incluyen asesoría por parte de nuestro equipo</p>
										</div>

										<div class="row">

										<div class="col-12 col-lg-12">

											<ul class="content-list">
											<li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" >Sistema Intermedio.</p>
											</li>
											<li class="flex item-center" >
												<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
												<p class="mb-0 ml-2" > App móvil.</p>
											</li>
											<li class="flex item-center" >
													<i class=" mb-0 clr-azul-claro fas fa-circle-notch"></i>
													<p class="mb-0 ml-2" >Página web/tienda virtual.</p>
												</li>
											</ul>

										</div>

										</div>

										</div>

										<div class="text-center mt-5">

										<a href="#Contacto" class="btn btn-plade text-white mt-4">¡Quiero saber más!</a>

										</div>
									</div>
								</div>
							</div>
							
					</div>
				</div>
			</div>
		</div>
	</div>
</section>