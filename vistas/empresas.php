
<section class="section pb-0">
	<div class="row">
		<div class="section-title col-12 text-center mb-4">
			<h3 class="mb-0" >Empresas que confian en Plade Software</h3>
		</div>
	</div>

	<div   class="main-carousel mb-5" data-flickity='{ "cellAlign": "left", "contain": true , "wrapAround": true , "freeScroll": true, "groupCells": true }'>
		<div class=" cell">
			<a href="https://instagram.com/legoagroproductivo?igshid=1rnm31b6qj4qy" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/1.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell flex flex-center">
			<a href="https://instagram.com/agroinsumosdon?igshid=ukumlmuao6pb" target="blank" class="img-servicio flex flex-center d-block mb-4">
				<img src="statics/images/logos/2.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell flex flex-center">
			<a href="https://www.gruporeigo.com/" target="blank" class="img-servicio flex flex-center d-block mb-4">
				<img src="statics/images/logos/3.png" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://instagram.com/agroelvalle?igshid=1i38pas7tpfvi" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/4.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://instagram.com/megafiestagre?igshid=1paqnrygdtxoi" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/5.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://instagram.com/msp.distribuciones?igshid=1miz96w80aeqv" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/6.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>


		<div class=" cell">
			<a href="https://www.bodegonpuntabrava.com/" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/7.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://instagram.com/roferca.ve?igshid=10nrtq7znfp4z" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/8.jpg" class="img-equipo" alt="Image" >
			</a>
			<!-- <h5><a href="#" class="text-white">Portones eléctricos</a></h5> -->
		</div>

		<div class=" cell">
			<a href="https://instagram.com/tbtgrill?igshid=19p4rjco2swya" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/9.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>
		
		<div class=" cell">
					<a href="https://www.wimaxsuperred.com/" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/10.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>
		
		<div class=" cell">
			<a href="https://instagram.com/ferreteriafermil?igshid=1s0scq6xvf00o" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/11.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>
		
		<div class=" cell">
			<a href="https://instagram.com/galan.center?igshid=160y4hgc57ii8" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/12.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>
		
		<div class=" cell">
			<a href="https://instagram.com/mas_automotriz?igshid=865gt1gzgi0v" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/13.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>
		
		<div class=" cell">
			<a href="https://instagram.com/tecnoberryca?igshid=115lzwlg06v1t" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/14.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://www.instagram.com/cocofresh.gre/?hl=es" target="blank" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/15.png" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="#D" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/16.png" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="#D" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/17.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://www.instagram.com/auto.dinamica.diesel/?hl=es" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/18.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://instagram.com/ferreconstrueyl?utm_medium=copy_link" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/19.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://instagram.com/mangodeorove?utm_medium=copy_link" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/20.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://instagram.com/mobiljetguanare?utm_medium=copy_link" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/21.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="#D" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/22.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://www.instagram.com/torogordoguanare/?hl=es" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/23.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="#D" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/24.jpeg" class="img-equipo" alt="Image" >
			</a>
		</div>

		<div class=" cell">
			<a href="https://instagram.com/zdcompany.ve?utm_medium=copy_link" class="img-servicio d-block mb-4">
				<img src="statics/images/logos/25.jpg" class="img-equipo" alt="Image" >
			</a>
		</div>
	</div>
</section>