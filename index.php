
<!DOCTYPE html>
<?php include_once('ruta.php'); ?>

<html lang="zxx">
	<head>
		<meta charset="utf-8">
		<title>Solución a la gestión administrativa de tu negocio - Plade Software</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="Si necesitas tener control de inventario, ventas, manejo de cuentas por cobrar y compras. Funciones avanzadas y personalizadas. ¡Tenemos la solución! Solicita tu demo.">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
		<meta name="author" content="Plade company">
		<meta name="keywords" content="Sistema administrativo,gestion de ventas">

		<link rel="shortcut icon" href="statics/images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="statics/plugins/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" href="statics/plugins/slick/slick.css">
		<link rel="stylesheet" href="statics/plugins/slick/slick-theme.css">
		<link rel="stylesheet" href="statics/plugins/fancybox/jquery.fancybox.min.css">
		<link rel="stylesheet" href="statics/plugins/fontawesome/css/all.min.css">
		<link rel="stylesheet" href="statics//animation/animate.min.css">
		<link rel="stylesheet" href="statics/plugins/jquery-ui/jquery-ui.css">
		<link rel="stylesheet" href="statics/plugins/timePicker/timePicker.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />  
		<!-- <link href="statics/css/style.min.css" rel="stylesheet"> -->
		<link href="statics/css/style.css" rel="stylesheet">
		<link rel="stylesheet" href="statics/css/flickity.css">
		<link rel="icon" href="static/images/favicon.png" type="image/x-icon">
		<script src="statics/plugins/jquery.min.js"></script>
	</head>


	<body>
		<div class="page-wrapper">
            

		<nav class="navbar ocultar-sm navbar-expand-lg bg-animated navbar-dark">
				<div class="container">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarLinks" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
				
					<div class="collapse navbar-collapse" id="navbarLinks">
						<ul class="navbar-nav flex flex-center item-center w-100 ">
							<li class="tag-inicio ">
								<img src="statics\images\logo-blanco.png" width="40px" alt="">
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio" data-toggle="collapse" data-target="#navbarLinks" aria-controls="navbarSupportedContent" aria-expanded="false" href="#Inicio">Inicio</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio" href="#Modulos">Nuestros módulos</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio" href="#Plade_app">Plade App</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio" href="#Paquetes">Paquetes</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio" href="https://pladecompany.com/quienes-somos/" target="_blank" >¿Quienes somos?</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio btn-header " href="#App">App vendedores</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<nav class="navbar ocultar-xl navbar-expand-lg bg-animated navbar-dark">
				<div class="container row ml-0 mr-0">
                 

				<div class="col-4">

				</div>
				<div class="col-4 flex flex-center">
				<img  src="statics\images\logo-blanco.png" width="40px" alt="">
				</div>
				<div class="col-4 text-right">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarLinks" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
				</div>
				

					
				
					<div class="collapse navbar-collapse" id="navbarLinks">
						<ul class="navbar-nav flex flex-center item-center w-100 ">
						
							<li class="tag-inicio">
								<a class="tag-inicio" href="#Inicio">Inicio</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio" href="#Modulos">Nuestros módulos</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio" href="#Plade_app">Plade App</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio" href="#Paquetes">Paquetes</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio" href="https://pladecompany.com/quienes-somos/" target="_blank" >¿Quienes somos?</a>
							</li>
							<li class="tag-inicio">
								<a class="tag-inicio btn-header " href="#App">App vendedores</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<?php include_once($ruta); ?>

			<footer class="footer-main">
				<div  class="footer-bottom bg-azul-oscuro w-100 ">
					<div class="container clearfix text-center flex flex-center ">
						<div class="copyright-text text-center ">
							<p class="text-white" >&copy; Copyright 2021. Diseñado &amp; Desarrollado por <a class="text-white" href="">Plasoftware CA J501752028 </a></p>
						</div>
					</div>
				</div>
			</footer>

			<div id="back-to-top" class="back-to-top">
				<i class="fa fa-angle-up"></i>
			</div>
		</div>

		<div class="scroll-to-top scroll-to-target" data-target=".header-top">
			<span class="icon fa fa-angle-up"></span>
		</div>
	
		<script src="statics/plugins/bootstrap/bootstrap.min.js"></script>
		<script src="statics/plugins/slick/slick.min.js"></script>
		<script src="statics/plugins/slick/slick-animation.min.js"></script>
		<script src="statics/plugins/fancybox/jquery.fancybox.min.js" defer></script>
		<script src="statics/plugins/php-email-form/validate.js" defer></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU" defer></script>
		<script src="statics/plugins/google-map/gmap.js" defer></script>
		<script src="statics/plugins/jquery-ui/jquery-ui.js" defer></script>
		<script src="statics/plugins/timePicker/timePicker.js" defer></script>
		<script src="statics/js/script.js"></script>
		<script src="statics/js/flickity.js"></script>
	</body>
</html>

