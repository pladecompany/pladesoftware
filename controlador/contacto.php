<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once __DIR__.'/../vendor/autoload.php';


//------ Cerrar conexion con el cliente.
ignore_user_abort(true);
set_time_limit(60);

ob_end_clean();
ob_start();

echo 'Tu mensaje ha sido enviado';
$size = ob_get_length();
header("Content-Length: $size\r\n");
header("Connection: Close\r\n");

http_response_code(200);
ob_flush();
ob_end_flush();
flush();
//--------------------------------------



// Pasar `true` habilita las excepciones.
$mail = new PHPMailer(true);
$mail->CharSet = 'UTF-8';
$mail->Encoding = 'base64';


try {
    //Server settings
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->setLanguage('es', '../vendor/phpmailer/phpmailer/language/');
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'mail.privateemail.com';                    //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'contacto@pladecompany.com';           //SMTP username
    $mail->Password   = base64_decode('cGxhZGUxNTUxIw==');    //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 465;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    // $mail->setFrom($_POST['correo'], $_POST['nombre']);
    $mail->setFrom('contacto@pladecompany.com', 'Contacto Plade Company');
    $mail->addReplyTo($_POST['correo'], $_POST['nombre']);
    $mail->addAddress('contacto@pladecompany.com', 'Contacto Plade Company');     //Add a recipient

    // $message = htmlspecialchars($_POST['message']);
    $ENDL = "<br>\n";

    $body = ('<b>Motivo:</b> ' . $_POST['asunto'] . $ENDL);
    $body .= ('<b>Nombre:</b> ' . $_POST['nombre'] ." ". $_POST['apellido'] . $ENDL);
    $body .= ('<b>Correo:</b> ' . $_POST['correo'] . $ENDL);
    $body .= ('<b>Whatsapp:</b> ' . $_POST['whatsapp'] . $ENDL);

    //Content
    // $mail->isHTML(false);                                  //Set email format to HTML
    $mail->Subject = $_POST['asunto'] . " | Contacto Plade Software";
    $mail->Body    = $body;
    $mail->AltBody = strip_tags($body);


    // Capturar salida.
    // ob_start();
    $mail->send();
    // ob_end_clean();

    // echo 'Message has been sent';
} catch (Exception $e) {
    error_log("No se envio el correo. Error: {$mail->ErrorInfo}");
    // echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}




//EOF